# Parse database configuration from $DATABASE_URL
import dj_database_url
import os
from os import environ

from .base import *
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ALLOWED_HOSTS = ['yourproject.example.com']

DATABASES = {
    'default': dj_database_url.config()
}

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
